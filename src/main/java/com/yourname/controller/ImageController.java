package com.yourname.controller;

import com.yourname.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/images")
@CrossOrigin(origins = "*")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/{imagename}", method = RequestMethod.GET)
    public String getImagesByName(@PathVariable("imagename") String name) {
        System.out.println(name);
        return imageService.getImagesByNames(name);
    }


}
